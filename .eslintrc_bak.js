module.exports = {
    "extends": "standard",
    "plugins": [
        "standard",
        "promise"
    ],
    "rules": {
        "semi": [
            "error",
            "always"
        ]
    },
    "globals": {
      "WebSocket": true,
      "__HTTPAPIHOST__": true,
      "__WSAPIHOST__": true,
      "XMLHttpRequest": true,
      "ActiveXObject": true,
      "RTCIceCandidate": true,
      "RTCSessionDescription": true,
      "RTCPeerConnection": true,
      "URL": true,
      "Blob": true,
      "FileReader": true
    }
};
