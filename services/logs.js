let log4js = require('log4js');

let path = require('path');
// log file rotation
let FileStreamRotator = require('file-stream-rotator');
let fs = require('fs');
let workDirectory = path.join(__dirname, '../');
let logDirectory = path.join(workDirectory, 'logs');
// ensure log directory exists
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

// create a rotating write stream
const accessLogStream = FileStreamRotator.getStream({
    date_format: 'YYYYMMDD',
    filename: path.join(logDirectory, 'access-%DATE%.log'),
    frequency: 'daily',
    verbose: false
});

let logAppenders =
    {
        type: 'dateFile',
        filename: path.join(logDirectory, 'basic.log'),
        pattern: 'yyyyMMdd',
        compress: false
    };

if (process.env.NODE_ENV === 'development') {
    logAppenders.push({type: 'console'});
}

log4js.configure(
    {
        appenders: {default: logAppenders},
        categories: {
            default: { appenders: ['default'], level: 'info' },
        }
    },
)

let getLogger = function (currentFilename) {
    return log4js.getLogger(currentFilename.substr(workDirectory.length));
};

module.exports = {
    accessLogStream: accessLogStream,
    getLogger: getLogger
};
