const wsService = require('./services/ws');
const signalingCoreService = require('./services/core');
const clientCoreService = require('./services/client');
const commonService = require('./services/common');
const callService = require("./services/call");
const messageService = require("./services/message");

class Vivar {}


let factory = function(userConfig = {}) {
    let loc = window.location;
    let config;
    let wprotocol = window.location.protocol === 'https:' ? 'wss:' : 'ws:';
    let defaultConfig = {
        httpHost: window.location.protocol + '//' + loc.host,
        wsHost: wprotocol + '//' + loc.host,
        p2p: true,
        audio: true,
        video: {
            width: { min: 640, ideal: 1280, max: 1920 },
            height: { min: 480, ideal: 720, max: 1080 }
        },
        sign: ''
    }

    config = Object.assign(defaultConfig, userConfig);

    let vivar = new Vivar();

    let videoWidth = 1280;
    let videoHeight = 720;

    vivar.config = config;
    vivar.joinGroups = {};
    vivar.config.videoWidth = videoWidth;
    vivar.config.videoHeight = videoHeight;

    vivar.me = {
        'state': 'offline' // current state : alive, busy, lost, offline
    };


    let RTCConnectionManager = new callService.RTCConnectionManager();
    let RTCConnection = callService.RTCConnection;

    messageService.setVivar(vivar);
    wsService.setVivar(vivar);
    signalingCoreService.setVivar(vivar, RTCConnectionManager, RTCConnection);
    clientCoreService.setVivar(vivar, RTCConnectionManager, RTCConnection);

    clientCoreService.init();
    wsService.init(vivar.config.wsHost, vivar.config.sign, vivar.me, vivar.joinGroups);

    commonService.mergeEventEmitterPrototype(vivar, ['Error', 'Close', 'Login', 'Logout',
        'JoinGroup', 'NotifyJoinGroup',
        'LeaveGroup', 'NotifyLeaveGroup',
        'UpdateGroup', 'NotifyUpdateGroup',
        'UpdateMe', 'UpdateProfileImg', 'UpdateStat', 'UpdateState', 'UpdateMyExt', 'UpdateConnectionState',
        'NotifyUpdateMember', 'NotifyUpdateState',
        'GetGroupMembers', 'GetMember', 'GetGroups', 'GetMyGroups', 'GetMyExt',
        'SendMessageDataChannel', 'CallDisconnected', 'CallConnected', 'IncomingCall', 'AcceptCall', 'RejectCall', 'SignalingFail',
        'ReceiveMessage', 'ReceiveAppState', 'ReceiveFileStart', 'ReceiveFileComplete', 'ReceiveFileOnProgress', 'ReceiveFileFail', 'ReceiveDrawingCaptureFile', 'ReceiveDrawingClear',
        'ReceiveDrawingTracking', 'ReceiveARDrawingReq', 'ReceiveARDrawingAdd', 'ReceiveTrackingResult', 'ReceiveTrackerType', 'ReceiveMemberLiveInfoReq', 'ReceiveMemberLiveInfoRes',
        'ReceiveARUndo', 'ReceiveARDrawingRemoveAllReq', 'ReceiveARRemoveAll', 'ReceiveARRemoveReq', 'ReceiveARRemove', 'ReceiveARFail', 'ReceiveColorChange',
        'ReceiveCameraFrameSizeReq', 'ReceiveCameraFrameSize', 'RecvScreenDrawingCanvasPoint',
        'UpdateMyCallHistory', 'CreateMyCallHistory', 'GetMyCallHistories', 'GetMyCallHistory', 'ChangePassword', 'ReceiveSwitchScreenShareReq', 'ReceiveSwitchScreenShareRes',
        'ReceiveSwitchComplete', 'ReceiveSwitchFail', 'ReceiveSwitchState'
    ]);

    wsService.onNotifyReceiveMessageFromMember = signalingCoreService.processSignalingProtocol

    return vivar;
};

module.exports = factory;