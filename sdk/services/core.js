require('webrtc-adapter');
require('mootools');
const messageService = require('./message');
const wsService = require('./ws');
const fileService = require("./file");
const fileManager = new fileService.FileManager();

let vivar = {};

let RTCConnectionManager;
let RTCConnection;

let sdpConstraints = {
	optional: [],
	mandatory: {
		OfferToReceiveAudio: true,
		OfferToReceiveVideo: true,
	}
};
let dataChannelOptions = {
	ordered: true, // 순서 보장 안함
	maxPacketLifeTime: 3000 // 밀리초 단위
};
let dataChannelOptions_poseMatrix = {
	ordered: true, // 순서 보장 안함
	maxRetransmits: 1
}

function processSignalingProtocol(fromId, messageType, msg) {
	if (!RTCConnectionManager.iceServer) {
		console.warn('RTCConnectionManager.iceServer: iceServer is not ready');
		return;
	}
	fromId = parseInt(fromId);
	let rtcConnection = RTCConnectionManager.getConnection(fromId);
	let message = JSON.parse(msg);

	switch (messageType) {
		case 'requestCall':
			console.log('[From:', fromId, '] requestCall : ', message);
			// 상대방으로 부터 통화 올때

			if (message.members.length === 0) {
				console.warn('requestGroupCall: requestCall is not ready')
				break;
			}
			if (RTCConnectionManager.getId() && RTCConnectionManager.getId() !== message.uuid) {
				// 통화중인데 다른그룹에서 requestCall 왔을때
				console.warn('[Group From:' + message.uuid + '] Other opponent Group requestCall Skip : ', message);
				// alert('Other opponent\'s requestCall Skip : ');
				break;
			}

			if (rtcConnection === undefined) {
				if (!RTCConnectionManager.getId()) { // id 가 없으면
					RTCConnectionManager.setId(message.uuid);
				}

				rtcConnection = new RTCConnection(vivar.me.id, fromId);
				RTCConnectionManager.setMembers(message.members);
				RTCConnectionManager.setGroup(message.group);

				RTCConnectionManager.add(rtcConnection);

				if (parseInt(vivar.me.id) < fromId) { // 자신보다 높은 id offer 큐에 담는다.
					let peer = createPeerConnection(rtcConnection);
					rtcConnection.setPeerConnection(peer);
					RTCConnectionManager.addOfferQueue(fromId);
					console.log('[RTCConnectionManager.addOfferQueue] --- ', RTCConnectionManager);
				}

				vivar.me.state = 'busy';
				wsService.postState();
				vivar.trigger('UpdateState', [vivar.me]);
				vivar.trigger('IncomingCall', [RTCConnectionManager.getMember(fromId), RTCConnectionManager.getMembers(), RTCConnectionManager.getGroup()]);
			} else {
				// do something
			}

			break;

		case 'inviteCall':
			console.log('[From:', fromId, '] inviteCall : ', message);
			// 상대방에게 통화 걸때
			let members = message.members;
			let group = message.group;
			if (members.length === 0) {
				console.warn('inviteCall: inviteCall is not ready');
				break;
			}
			if (RTCConnectionManager.getId() && RTCConnectionManager.getId() !== message.uuid) {
				// 통화중인데 다른그룹에서 inviteCall 왔을때
				console.warn('[Group From:' + message.uuid + '] Other opponent Group inviteCall Skip : ', message);
				// alert('Other opponent\'s acceptCall Skip : ');
				break;
			}

			if (rtcConnection === undefined) {
				if (!RTCConnectionManager.getId()) { // id 가 없으면
					RTCConnectionManager.setId(message.uuid);
				}

				rtcConnection = new RTCConnection(vivar.me.id, fromId);
				RTCConnectionManager.setMembers(members);
				RTCConnectionManager.setGroup(group);
				RTCConnectionManager.add(rtcConnection);
			}

			if (parseInt(vivar.me.id) < fromId) { // 자신보다 높은 id offer queue에 담는다.
				let rtcConnection = RTCConnectionManager.getConnection(fromId);
				if (rtcConnection === undefined) {
					rtcConnection = new RTCConnection(vivar.me.id, fromId);
					RTCConnectionManager.add(rtcConnection);
				}
				let peer = createPeerConnection(rtcConnection);
				rtcConnection.setPeerConnection(peer);
				RTCConnectionManager.addOfferQueue(fromId);
				console.log('[RTCConnectionManager.addOfferQueue] --- ', RTCConnectionManager);
			}

			vivar.me.state = 'busy';
			wsService.postState();
			vivar.trigger('UpdateState', [vivar.me]);
			vivar.trigger('IncomingCall', [RTCConnectionManager.getMember(fromId), RTCConnectionManager.getMembers(), RTCConnectionManager.getGroup()]);
			break;
		case 'acceptCall':
			console.log('[From:', fromId, '] acceptCall : ', message);
			// 상대가 통화 승인했을때
			if (!RTCConnectionManager.getId() || RTCConnectionManager.getId() !== message.uuid) {
				// 다른 그룹에서 acceptCall 왔을 때
				console.warn('[Group From:' + message.uuid + '] Other opponent Group acceptCall Skip : ', message);
				// alert('Other opponent\'s acceptCall Skip : ');
			} else {
				if (rtcConnection === undefined) {
					if (!RTCConnectionManager.getId()) { // id 가 없으면
						RTCConnectionManager.setId(message.uuid);
					}

					rtcConnection = new RTCConnection(vivar.me.id, fromId);
					let members = message.members;
					if (RTCConnectionManager.getMembers().length !== members.length) { //todo length로 하지 말자 - 초대하는 경우
						// 이미 통화 연결된 사람이(방장이 아닌) 초대 수락한 사람에게 나 이미 통화중이라고 알려주는 상황
						let member = members.find((member) => {
							return parseInt(member.id) === fromId;
						});
						RTCConnectionManager.setMember(member);

						let group = RTCConnectionManager.getGroup();
						group.extra = JSON.stringify(group.extra);
						let message = { uuid: RTCConnectionManager.getId(), group: group, members: members };
						wsService.sendMessageToMember(member.id, 'acceptCall', JSON.stringify(message), Date.now());
						console.log('[to:', member.id, '] acceptCall send : ', message);

					}
					RTCConnectionManager.add(rtcConnection);
				} else {
					//todo do something
				}

				if (parseInt(vivar.me.id) < fromId) { // 자신보다 높은 id offer queue에 담는다.
					let peer = createPeerConnection(rtcConnection);
					rtcConnection.setPeerConnection(peer);
					RTCConnectionManager.addOfferQueue(fromId);
					console.log('[RTCConnectionManager.addOfferQueue] --- ', RTCConnectionManager);
				}

				if (RTCConnectionManager.isReadytoCallConnect) {
					doOffer();
				}
				vivar.trigger('AcceptCall', [RTCConnectionManager.getMember(fromId)]);
			}

			break;
		case 'cancelCall':
			console.log('[From:', fromId, '] Cancel Call : ', message);

			// 내가 통화 취소했을때
			if (!RTCConnectionManager.getId() || RTCConnectionManager.getId() !== message.uuid) {
				// 다른 그룹에서 cancelCall 왔을 때
				console.warn('[Group From:' + message.uuid + '] Other opponent Group cancelCall Skip : ', message);
				// alert('Other opponent\'s cancelCall Skip : ');
			} else {
				let member = RTCConnectionManager.getMember(fromId);
				let isCaller = member.isCaller;
				RTCConnectionManager.removeConnection(fromId);
				if (Object.keys(RTCConnectionManager.connections).length === 0 || isCaller) {
					// 연결된 객체가 더이상 없을 때
					initCallConfig();
					vivar.trigger('CallDisconnected', [null, true]);
				} else {
					vivar.trigger('CallDisconnected', [rtcConnection, false]);
				}
			}

			break;
		case 'rejectCall':
			console.log('[From:', fromId, '] Reject Call : ', message);

			// 상대가 통화 거절했을때
			if (!RTCConnectionManager.getId() || RTCConnectionManager.getId() !== message.uuid) {
				// 다른 그룹에서 rejectCall 왔을 때
				console.warn('[Group From:' + message.uuid + '] Other opponent Group rejectCall Skip : ', message);
				//alert('Other opponent\'s rejectCall Skip : ');
			} else {
				let member = RTCConnectionManager.getMember(fromId);
				let isCaller = member.isCaller;
				RTCConnectionManager.removeConnection(fromId);
				if (Object.keys(RTCConnectionManager.connections).length === 0 || isCaller) {
					// 연결된 객체가 더이상 없을 때
					initCallConfig();
					vivar.trigger('RejectCall', [member, true]);
				} else {
					vivar.trigger('RejectCall', [member, false]);
				}
			}

			break;
		case 'endCall':
			console.log('[From:' + fromId + '] End Call : ' + message + ', remoteUserId : ' + fromId);

			// 통화중 , 상대가 통화 종료 했을때
			if (!RTCConnectionManager.getId() || RTCConnectionManager.getId() !== message.uuid) {
				// 다른 그룹에서 endCall 왔을 때
				console.warn('[Group From:' + message.uuid + '] Other opponent Group endCall Skip : ', message);
				// alert('Other opponent\'s endCall Skip : ');
			} else {
				if (rtcConnection === undefined) {
					console.warn(`endCall -[From: ${fromId}] already this rtcConnection removed`);
					break;
				}

				if (rtcConnection && fromId === rtcConnection.remoteUserId) {
					let member = RTCConnectionManager.getMember(fromId);
					let isCaller = member.isCaller;
					RTCConnectionManager.removeConnection(fromId);

					if (Object.keys(RTCConnectionManager.connections).length === 0 || isCaller) {
						// 연결된 객체가 더이상 없거나 방장이 전화 끊었을 때
						initCallConfig();
						vivar.trigger('CallDisconnected', [null, true]);
					} else {
						vivar.trigger('CallDisconnected', [rtcConnection, false]);
					}
				}
			}

			break;
		case 'signalingFail':
			console.log('[From:', fromId, '] signalingFail : ', message);

			//  방장만 할 수 있는 권한. 상대방이 응답이 없을 경우(ex.폰이 꺼져 있는 상태) 다른 상대한테 알려주는
			if (!RTCConnectionManager.getId() || RTCConnectionManager.getId() !== message.uuid) {
				// 다른 그룹에서 signalingFail 왔을 때
				console.warn('[Group From:' + message.uuid + '] Other opponent Group signalingFail Skip : ', message);
				//alert('Other opponent\'s rejectCall Skip : ');
			} else {
				let rtcConnection = RTCConnectionManager.getConnection(message.userId);
				let member = RTCConnectionManager.getMember(message.userId);
				let isCaller = member.isCaller;
				RTCConnectionManager.removeConnection(message.userId);
				if (Object.keys(RTCConnectionManager.connections).length === 0 || isCaller) {
					// 연결된 객체가 더이상 없을 때
					initCallConfig();
					vivar.trigger('CallDisconnected', [null, true]);
				} else {
					vivar.trigger('CallDisconnected', [rtcConnection, false]);
				}
			}

			break;
		case 'sdp':
			if (rtcConnection && rtcConnection.remoteUserId === fromId) {
				if (RTCConnectionManager.isReadytoCallConnect) {
					let remoteSdp = message;

					if (remoteSdp.type === 'offer') {
						if (rtcConnection.getPeerConnection()) { // 이미 peerConnection set 되어있을때 (똑같은 offer가 두번 왓을때)
							console.warn('[From:', fromId, '][ Already Remote peerConnection Set : ');
						} else {
							let peer = createPeerConnection(rtcConnection);
							rtcConnection.setPeerConnection(peer);
						}

						rtcConnection.addLocalStream(RTCConnectionManager.getLocalStream());

						rtcConnection.setRemoteSDP(remoteSdp).then(() => {
							doAnswer(rtcConnection);
							rtcConnection.isRemoteSDPSet = true;
							rtcConnection.registerCandidate();
							console.log('[From:', fromId, '][callFrom] Remote SDP Set : ', remoteSdp);
						});
					} else if (remoteSdp.type === 'answer') {
						rtcConnection.setRemoteSDP(remoteSdp).then(() => {
							rtcConnection.isRemoteSDPSet = true;
							rtcConnection.registerCandidate();
							console.log('[From:', fromId, '][callTo] Remote SDP Set : ', remoteSdp);

						}).catch(onfailure);
					} else {
						console.warn('[From:', fromId, '] abnormal SDP message  : ', message);
					}

				} else {
					//todo do something
				}
			} else {
				console.warn('[From:', fromId, '][callFrom] Other opponent\'s SDP Skip : ', message);
			}
			break;
		case 'candidate':
			if (rtcConnection && rtcConnection.remoteUserId === fromId) {
				let candidate = message;
				rtcConnection.registerCandidate(candidate).then(() => {
					console.log('[From:', fromId, '] Remote candidate Set : ', candidate);
				}, onfailure)
			} else {
				console.warn('[From:', fromId, '] Other opponent\'s candidate Skip : ', message);
			}
			break;

		default:
			console.warn('Message not Processed [messageType:' + messageType + ']', message);
			break;
	}
}

let onfailure = function(error) {
	console.log('doOffer onfailure');
	vivar.trigger('Error', [error]);
};

function doOffer() {
	let rtcConnectionOfferQueue = RTCConnectionManager.offerQueue;
	console.log('RTCConnectionManager.offerQueue ---- ', RTCConnectionManager.offerQueue);

	while (rtcConnectionOfferQueue.length > 0) {
		let nextOfferId = RTCConnectionManager.getNextOfferCycle();
		let rtcConnection = RTCConnectionManager.getConnection(nextOfferId);

		if (rtcConnection) {
			rtcConnection.addLocalStream(RTCConnectionManager.getLocalStream());

			rtcConnection.createOffer((offerSDP) => {
				let arr = offerSDP.sdp.split('\r\n');
				arr.forEach((str, i) => {
					if (/^a=fmtp:\d*/.test(str)) {
						arr[i] = str + ';x-google-max-bitrate=2500';
					} else if (/^a=mid:(1|video)/.test(str)) {
						arr[i] += '\r\nb=AS:2500';
					}
				});
				offerSDP = new RTCSessionDescription({
					type: 'offer',
					sdp: arr.join('\r\n'),
				});

				rtcConnection.setLocalDescription(offerSDP).then(function() {
					wsService.sendMessageToMember(rtcConnection.remoteUserId, 'sdp', JSON.stringify(offerSDP), Date.now());
					console.log('[to:', rtcConnection.remoteUserId, '] offer SDP send : ', offerSDP);
				});
			}, onfailure, sdpConstraints)
		}
	}
}

function doAnswer(rtcConnection) {
	rtcConnection.createAnswer((answerSDP) => {
		let arr = answerSDP.sdp.split('\r\n');
		arr.forEach((str, i) => {
			if (/^a=fmtp:\d*/.test(str)) {
				arr[i] = str + ';x-google-max-bitrate=2500';
			} else if (/^a=mid:(1|video)/.test(str)) {
				arr[i] += '\r\nb=AS:2500';
			}
		});
		answerSDP = new RTCSessionDescription({
			type: 'answer',
			sdp: arr.join('\r\n'),
		});
		rtcConnection.setLocalDescription(answerSDP).then(function() {
			wsService.sendMessageToMember(rtcConnection.remoteUserId, 'sdp', JSON.stringify(answerSDP), Date.now());
			console.log('[to:', rtcConnection.remoteUserId, '] Answer SDP send : ', answerSDP);
		});
	}, onfailure, sdpConstraints);
}

function createPeerConnection(rtcConnection) {

	let iceServers = RTCConnectionManager.getIceServer();

	if (!iceServers) { // iceServer가 없으면
		console.warn('createPeerConnection -- IceServer not ready')
		return;
	}
	let peer = new RTCPeerConnection(iceServers);
	peer.ontrack = function(event) {
		console.log('on Track call back', event)
		rtcConnection.addRemoteStream(event.streams[0])
	};

	peer.onremovetrack = function() {
		console.log('on Remove Track call back', arguments)
	};
	peer.oniceconnectionstatechange = function() {
		console.log('oniceconnectionstatechange : ', peer.iceConnectionState);
		if (peer.iceConnectionState === 'failed' || peer.iceConnectionState === 'closed') {
			rtcConnection.callState = peer.iceConnectionState;
			vivar.trigger('UpdateConnectionState', [rtcConnection]);
		} else if (peer.iceConnectionState === 'disconnected') {
			rtcConnection.callState = 'disconnected';
			vivar.trigger('UpdateConnectionState', [rtcConnection]);
		} else if (peer.iceConnectionState === 'connected') {
			rtcConnection.callState = 'connected';
			vivar.trigger('CallConnected', [RTCConnectionManager.getMember(rtcConnection.remoteUserId), RTCConnectionManager.getMembers(), rtcConnection.peerClient.remoteVideo]);
		}

	};

	peer.onnegotiationneeded = function(event) {
		console.log('onnegotiationneeded', event);
		// doOffer();  //재 Offer  //TODO 받은 쪽에서 다시 재 Answer 해 주어야 한다고 하는데.. 활성화 시키면 중간에 에러 나고, datachnnel도 이상해짐..
	};

	peer.onsignalingstatechange = function(event) {
		console.log('onsignalingstatechange', event);
	};

	peer.onicecandidate = function(event) {

		var candidate = event.candidate;
		if (candidate && vivar.config.p2p) {
			wsService.sendMessageToMember(rtcConnection.remoteUserId, 'candidate', JSON.stringify(candidate), Date.now());
			console.log('[to:', rtcConnection.remoteUserId, '] candidate send : ', candidate);
		} else if (candidate && !vivar.config.p2p) {
			if (candidate.candidate.includes('typ relay')) {
				wsService.sendMessageToMember(rtcConnection.remoteUserId, 'candidate', JSON.stringify(candidate), Date.now());
				console.log('[to:', rtcConnection.remoteUserId, '] candidate send : ', candidate);
			}
		} else {
			console.log('candidate all gathering ---- ', candidate);
		}
	};

	peer.receiveMessageDataChannel = peer.createDataChannel('MessageChannel', dataChannelOptions);
	peer.receiveMessageDataChannel.onmessage = messageService.handleRemoteDataChannelOnMessage.bind(messageService);
	peer.receiveMessageDataChannel.onopen = function(e) {
		console.log('receiveMessageDataChannel onopen', e);
	};
	peer.receiveMessageDataChannel.onclose = function(e) {
		console.log('receiveMessageDataChannel onclose', e);
	};
	peer.receiveMessageDataChannel.onerror = function(e) {
		console.error('receiveMessageDataChannel onerror', e);
	};

	peer.receiveFileDataChannel = peer.createDataChannel('FileTransportChannel', dataChannelOptions);
	peer.receiveFileDataChannel.binaryType = 'arraybuffer';
	peer.receiveFileDataChannel.onmessage = function(e) {
		let fileReceiver = fileManager.receiveFileOnDataChannel(e);
		if(fileReceiver.state === 'init') {
			console.log('ReceiveFileStart')
			vivar.trigger('ReceiveFileStart', [fileReceiver.uuid, rtcConnection.remoteUserId, fileReceiver.meta.name, fileReceiver.meta.mimeType]);
		}
		fileReceiver.onProgress = function(cSize, fileSize) {
			let percent = Math.floor((cSize / fileSize) * 100);
			console.log('receive File onProgress - ', percent)
			if (percent % 5 === 0 ) {
				vivar.trigger('ReceiveFileOnProgress', [fileReceiver.uuid, rtcConnection.remoteUserId, fileReceiver.meta.name, fileReceiver.meta.mimeType, percent]);
			}
		}
		fileReceiver.onCompleted = function() {
			console.log('[' + fileReceiver.id + '] 파일 전송 완료');
			fileManager.clearAll();
			vivar.trigger('ReceiveFileComplete', [fileReceiver.uuid, rtcConnection.remoteUserId, fileReceiver.meta.name, fileReceiver.meta.mimeType, fileReceiver.getByteArray()]);
		}
		fileReceiver.onError = function (id) {
			fileManager.removeReceiver(id);
			vivar.trigger('ReceiveFileFail', [fileReceiver.uuid, rtcConnection.remoteUserId, fileReceiver.meta.name, fileReceiver.meta.mimeType])
		}
	}
	peer.receiveFileDataChannel.onopen = function(e) {
		console.log('receiveFileDataChannel onopen', e);
	};
	peer.receiveFileDataChannel.onclose = function(e) {
		console.log('receiveFileDataChannel onclose', e);
	};
	peer.receiveFileDataChannel.onerror = function(e) {
		console.error('receiveFileDataChannel onerror', e);
	};

	peer.receivePoseMatrixChannel = peer.createDataChannel('PoseMatrixChannel', dataChannelOptions_poseMatrix);
	peer.receivePoseMatrixChannel.onmessage = messageService.handleRemoteDataChannelOnMessage.bind(messageService);
	peer.receivePoseMatrixChannel.onopen = function(event) {
		// console.log('receivePoseMatrixChannel onopen', e);
	};
	peer.receivePoseMatrixChannel.onclose = function(e) {
		// console.log('receivePoseMatrixChannel onclose', e);
	};
	peer.receivePoseMatrixChannel.onerror = function(e) {
		console.error('receivePoseMatrixChannel onerror', e);
	};

	peer.ondatachannel = function(event) {
		sendDataChannelCallBack(event, rtcConnection); // 상대로부터 받은 데이터 채널
	};

	/*// 초당 대역폭(sent / received) 콘솔
	var prevReportSent = null, prevReportRecieved = null;
	var t = setInterval(function() {
		if (!peer) {
			prevReportSent = null;
			prevReportRecieved = null;
			return;
		}
		peer.getStats(null).then(reporter => {
			reporter.forEach(report => {
				if(report.id.includes('RTCOutboundRTPVideoStream')) {
					if (report.type === 'outbound-rtp' && report.mediaType === 'video') {
						if (prevReportSent) {
							let bps = ((report.bytesSent - prevReportSent.bytesSent)/(report.timestamp - prevReportSent.timestamp)).toFixed(1);
							if(bps > 0) {
								// console.log('send', report);
								console.log('send: ', bps + ' bps');
							}
						}
						if(report.bytesSent != 0) prevReportSent = report;
						else prevReportSent = null;
					}
				}
				if(report.id.includes('RTCInboundRTPVideoStream')) {
					if (report.mediaType === 'video') {
						if (prevReportRecieved) {
							let bps = ((report.bytesReceived - prevReportRecieved.bytesReceived) / (report.timestamp - prevReportRecieved.timestamp)).toFixed(1);
							if(bps > 0) {
								// console.log('received', report);
								console.log('received: ', bps + ' bps');
							}
						}
						if (report.bytesReceived != 0) prevReportRecieved = report;
						else prevReportRecieved = null;
					}
				}
			});
		});
	}, 1000);*/

	return peer;
}


function sendDataChannelCallBack(event, rtcConnection) {
	//TODO 이부분 리팩토링 필요
	console.log('sendDataChannelCallBack:', event.channel, event.channel.label);
	if (event.channel.label === 'MessageChannel') {
		rtcConnection.sendMessageDataChannel = event.channel;

		rtcConnection.sendMessageDataChannel.onmessage = function(e) {
			console.log('sendMessageDataChannel onmessage', e);
		};
		rtcConnection.sendMessageDataChannel.onopen = function(e) {
			vivar.trigger('SendMessageDataChannel', [rtcConnection.remoteUserId]);
			console.log('sendMessageDataChannel onopen', e);
		};
		rtcConnection.sendMessageDataChannel.onclose = function(e) {
			console.log('sendMessageDataChannel onclose', e);
		};
		rtcConnection.sendMessageDataChannel.onerror = function(e) {
			console.error('sendMessageDataChannel onerror', e);
		};
	} else if (event.channel.label === 'FileTransportChannel') {
		rtcConnection.sendFileDataChannel = event.channel;

		rtcConnection.sendFileDataChannel.onmessage = function(e) {
			console.log('sendFileDataChannel onmessage', e);
		};
		rtcConnection.sendFileDataChannel.onopen = function(e) {
			console.log('sendFileDataChannel onopen', e);
		};
		rtcConnection.sendFileDataChannel.onclose = function(e) {
			console.log('sendFileDataChannel onclose', e);
		};
		rtcConnection.sendFileDataChannel.onerror = function(e) {
			console.error('sendFileDataChannel onerror', e);
		};
	} else if (event.channel.label === 'PoseMatrixChannel') {
		rtcConnection.sendPoseMatrixDataChannel = event.channel;

		rtcConnection.sendPoseMatrixDataChannel.onmessage = function(e) {
			console.log('sendPoseMatrixDataChannel onmessage', e);
		};
		rtcConnection.sendPoseMatrixDataChannel.onopen = function(e) {
			vivar.trigger('sendPoseMatrixDataChannel', [rtcConnection.remoteUserId]);
			console.log('sendPoseMatrixDataChannel onopen', e);
		};
		rtcConnection.sendPoseMatrixDataChannel.onclose = function(e) {
			console.log('sendPoseMatrixDataChannel onclose', e);
		};
		rtcConnection.sendPoseMatrixDataChannel.onerror = function(e) {
			console.error('sendPoseMatrixDataChannel onerror', e);
		};
	}
}


function initCallConfig() {
	RTCConnectionManager.init();
	fileManager.clearAll(); // 파일 매니저 초기화
	vivar.me.state = 'alive';
	wsService.postState();
	vivar.trigger('UpdateState', [vivar.me]);
}
function setIceServer(servers) {
	RTCConnectionManager.setIceServer(servers);
}
module.exports = {
	setVivar: function (paramVivar, rtccm, rtcc) {
		vivar = paramVivar;
		RTCConnectionManager = rtccm;
		RTCConnection = rtcc;
	},
	setIceServer: setIceServer,
	doOffer: doOffer,
	doAnswer: doAnswer,
	processSignalingProtocol: processSignalingProtocol,
};