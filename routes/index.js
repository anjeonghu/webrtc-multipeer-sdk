let express = require('express');
let router = express.Router();
let path = require('path');

// devel raw page
router.get('/', function (req, res, next) {
  res.sendFile(path.join(__dirname, '../public/raw.html'));
});
// router.get('/raw', function (req, res, next) {
//   res.render('index', { title: 'Vivar' });
// });


module.exports = router;
