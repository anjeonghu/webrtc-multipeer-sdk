'use strict';
var loc = window.location;

var config = {};
var prePose = [];
function init () {
    config.localVideo = $('#local-video1')[0];
    config.remoteVideo = document.createElement('video');

    // 진입
    $('.callTo').hide();
    $('.callFrom').hide();
    $('.data').hide();
    $('#disconnect').hide();
    $('#groupBtnWrapper').hide();
};
init();



// 배포용
//var baseUrl = '';

// 테스트용
var baseUrl = 'https://dev.msf.maxst.com';
config.httpHost = 'https://dev.msf.maxst.com';
config.wsHost = 'wss://lydia.gbrandar.com';


var vivar;
var callMembers = [];
var members;
var myStream = null;
var group;

function logoutUIProcess () {
    $('#loginbtn').attr('disabled', 'disabled');
    $('#callTo').attr('disabled', 'disabled');

    setTimeout(function () {
        init();

        $('#loginbtn').removeAttr('disabled');
        $('#callTo').removeAttr('disabled');
        $('#myId').removeAttr('disabled');
        $('#loginbtn').text('login');
    }, 500);
}

// 로그인 후
// 전화 걸 수 있게 화면 변경

$('#loginbtn').click(function () {
    if (!vivar) {
        vivar = new Vivar(config);
    }
    if (vivar.me.state === 'offline') {
        var userId = $('#myId').val();
        var userPw = $('#myPw').val();

        if (!userId || !userPw) {
            alert('로그인 아이디와 비번을 설정해 주세요.');
            return;
        }

        // vivar event callback 등록
        vivar.onError = function (error) {
            // console.error('onError', error);
        };
        vivar.onCreateMyCallHistory = function (result, body) {
            console.log('onCreateMyCallHistory', body)
        }
        vivar.onReceiveTrackingResult = function (posMatrix) {
            if (posMatrix) {
                prePose =  posMatrix;
            }
        }

        vivar.onReceiveARDrawingAdd = function (result) {
            //console.log('onReceiveARDrawingAdd ---  ',result)
        }

        vivar.onReceiveARDrawing = function (id, stroke, anchorPose) {
            //console.log('ReceiveARDrawing -- anchorPose', anchorPose);
        }
        vivar.onReceiveARRemoveAll = function () {
            //console.log('ReceiveARRemoveAll -- anchorPose', anchorPose);
        }
        vivar.onLogin = function (result) {
            console.log('onLogin', result, vivar.me);
            if (result === true) {
                $('.login_wrapper').hide();
                $('.video-wrapper').show();
                $('.callTo').show();

                vivar.requestPCScreenShare().then((stream) => {
                    myStream = stream;
                    let localVideo = document.getElementById('local-video1');
                    localVideo.srcObject = null;

                    localVideo.muted = true;

                    localVideo.srcObject = myStream;
                    localVideo.play().then(() => {
                    }).catch(e => {
                        console.log(e)
                    });
                })
            }
        };
        vivar.onJoinGroup = function (result, body) {
            console.log('onJoinGroup', result, body);
            group = body;
            vivar.getGroupMembers(group.id);
        };
        vivar.onNotifyJoinGroup = function (groupId, member) {
            console.log('onNotifyJoinGroup', groupId, member);
        };
        vivar.onNotifyLeaveGroup = function (groupId, member) {
            //console.log('onNotifyLeaveGroup', groupId, member);
        };
        vivar.onUpdateMe = function (result, body) {
            // console.log('onUpdateMe', result, body);
        };
        vivar.onNotifyUpdateMember = function (groupId, member) {
            console.log('onNotifyUpdateMember', groupId, member);
        };
        vivar.onNotifyUpdateState = function (groupId, member) {
            console.log('onNotifyUpdateState', groupId, member);
            vivar.getGroupMembers(groupId);
        };
        vivar.onGetGroupMembers = function (result, body) {
            console.log('onGetGroupMembers', result, body);
            members = body.members;
            let $ele = $('#userList');
            $ele.empty();
            Object.values(members).forEach(function (member) {
                let ele = document.createElement('option');
                ele.value= JSON.stringify(member);
                ele.innerText = member.memberId;
                $ele.append(ele)
            })
        };
        vivar.onIncomingCall = function (remoteMember, members, workspaceInfo) {
            console.log('onIncomingCall', remoteMember);
            $('.callFrom').show();
            $('.callFrom').text('Accept Call From ' + remoteMember.memberId);
        };
        vivar.onRejectCall = function (fromId) {
            //   console.log('onRejectCall', fromId);
            vivar.endCall();
            disconnect();
        };
        vivar.onCallDisconnected = function (rtcConnection) {
            // console.log('onCallDisconnected');
            callMembers = [];
            $('.callFrom').hide();
            $('.data').hide();
            $('#disconnect').hide();
            $('#groupBtnWrapper').hide();
            try {
                document.getElementById('container').removeChild(rtcConnection.peerClient.remoteVideo);
            }catch (e){
                console.log('not exsist video node')
            }

        };

        vivar.onCallConnected = function (member, callMembers) {
            console.log('onCallConnected', callMembers);
            $('#video-wrapper').removeClass('disconnected');
            $('#video-wrapper').addClass('connected');
            $('#container').addClass('connected');
            $('.btn-group').addClass('connected');
            $('#groupBtnWrapper').show();

            $('.callFrom').hide();

            let localVideo = document.getElementById('local-video1');
            localVideo.srcObject = null;

            let stream;
            if (member.isWorker) {
                // 화면 공유자면 remoteVideo에 stream 세팅
                stream = vivar.getMemberStream(member.id);
                localVideo.srcObject = stream;
                localVideo.play().then(() => {
                }).catch(e => {
                    console.log(e)
                });
            }
        };
        vivar.onReceiveMessage = function (message) {
            $('.messages-content').append("<div class='message message-personal'>" + message + '</div>');
            $('.messages-content ').scrollTop(310);
        };
        vivar.onReceiveState = function (target, state) {
            $('.messages-content').append("<div class='message message-personal'>" + target + ' ' + state + '</div>');
            $('.messages-content ').scrollTop(310);
        };
        vivar.onReceiveFile = function (name, contentType, byteArray) {
            let downloadAnchor = document.createElement('a');

            let blob = new Blob([byteArray], {type: contentType})
            downloadAnchor.setAttribute('href', window.URL.createObjectURL(blob));

            downloadAnchor.download = name;
            downloadAnchor.textContent = 'Click to download \'' + name;
            downloadAnchor.style.display = 'block';

            let btnGroup = document.querySelector('.btn-group')
            btnGroup.appendChild(downloadAnchor)
        };
        vivar.onReceiveDrawingClear = function () {
            console.log('onReceiveDrawingClear');
        };
        vivar.onReceiveDrawingTracking = function (result, polyLines) {
            console.log('onReceiveDrawingTracking', result, polyLines);
        };
        console.log('로그인 요청');

        axios.post(`${baseUrl}/api/token`, {loginId:userId, pw: userPw})
            .then( response => {
                vivar.loginFactory(response.data.token, true);
            })
            .catch( error => {
                alert(error)
            });
        $('#myId').attr('disabled', 'disabled');
        $('#loginbtn').text('logout');
    } else if (vivar.me.state === 'busy') {
        alert('전화를 먼저 끊어 주세요.');
    } else {
        console.log('로그아웃 요청');
        vivar.logout();
    }
});

// 전화를 할 경우
$('#callTo').on('click', function () {
    // 전화를 할 경우  (그룹 전화 테스트)
    // $('#remote').hide();
    $('.signal-wrap').show();

    if (!callMembers.length) {
        alert('멤버를 선택해 주세요.');
        return;
    }
    // let remotes =
    //     [
    //         {"state":"alive","id":"2","memberId":"supporter02","extra":"","isWorker":false},
    //         {"state":"alive","id":"3","memberId":"supporter03","extra":"","isWorker":true},
    //         {"state":"alive","id":"4","memberId":"supporter04","extra":"","isWorker":false}
    //
    //     ]
    vivar.me.isWorker = true;
    callMembers.push(vivar.me)
    vivar.sendCall(callMembers);
    console.log(vivar.createMyCallHistory('Out', callMembers));

    $('#disconnect').show();
    $('.data').show();
});

// 멤버 선택
$('#userList').click(function (event) {
    console.log(event.target.value);
    callMembers.push(JSON.parse(event.target.value));
});
// 전화가 온 경우
$('#callFrom').click(function () {
    vivar.acceptCall();
    $('#disconnect').show();
    $('.data').show();
});

$('#message').click(function () {
    if ($('.chat').hasClass('show')) {
        $('.chat').removeClass('show');
    } else {
        $('.chat').addClass('show');
    }
});

$('#sendDatabtn').click(function () {
    let message = $('#sendData').val();
    vivar.sendMessage(message);

    $('.messages-content').append("<div class='message'>" + message + '</div>');
    $('#sendData').val('');
    $('#sendData').focus();
    $('.messages-content ').scrollTop(320);
});
let onoffTestCnt = 0;
$('#onoffTestbtn').click(function () {
    onoffTestCnt++;
    if (onoffTestCnt === 1) {
        vivar.enableVideo(false);
    } else if (onoffTestCnt === 2) {
        vivar.enableVideo(true);
    } else if (onoffTestCnt === 3) {
        vivar.enableAudio(false);
    } else {
        vivar.enableAudio(true);
        onoffTestCnt = 0;
    }
});
$('#fileTrasfer').click(function () {
    $('#upload').trigger('click');
});
$('#upload').change(function () {
    vivar.sendFile($('#upload')[0].files);
});

$('#proffileTrasfer').click(function () {
    $('#profupload').trigger('click');
});
$('#profupload').change(function () {
    vivar.updateProfileImg($('#profupload')[0].files[0]);
});

function disconnect () {
    callMembers = [];

    $('#sendDataMessage').text('');
    $('#recvData').text('');
    $('#upload').val('');
    $('#profupload').val('');
    $('.signal-wrap').hide();

    $('#callTo').removeAttr('disabled');
    $('#receiveFrom').removeAttr('disabled');
    $('.callFrom').hide();
    $('#disconnect').hide();
    $('.data').hide();
    $('#video-wrapper').removeClass('connected');
    $('#container').removeClass('connected');
    $('.btn-group').removeClass('connected');

    let sendProgress = $('#sendProgress')[0];
    sendProgress.value = 0;
    sendProgress.max = 0;

    let downloadAnchor = $('#download')[0];
    downloadAnchor.href = '';
    downloadAnchor.download = '';
    downloadAnchor.textContent = '';
    downloadAnchor.style.display = 'block';
}

$('#onArPreparebtn').click(function () {
    vivar.sendARPrepare();
})
$('#onSendDrawReqbtn').click(function () {
    var obj = {};
    obj.id = '1111';
    obj.stroke = {};
    obj.stroke.line = [{x: 10, y: 10}, {x: 50, y: 20}, {x: 100, y: 100}];
    obj.stroke.color = {r: 255, g: 0, b: 0, a: 255};
    obj.stroke.width = 5;
    obj.prevPose = prePose;
    vivar.sendARDrawing(obj);
})
$('#onSendClearbtn').click(function () {
    vivar.sendARRemoveAll();
})
$('#disconnect').click(function () {
    callMembers = [];
    vivar.endCall();
    $('.callFrom').hide();
    $('.data').hide();
    $('#disconnect').hide();
    $('#groupBtnWrapper').hide();
});
