const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = function (environment) {
  let libPath = path.resolve(__dirname, 'lib');

  let devtool = 'inline-source-map';
  // let babiliConfig = ['babili', {  // uglify는 es2015등을 지원 제대로 못해서 이걸로 사용
  //   'builtIns': false // mootolls에서 에러 발생해서 여기서는 사용 안 함
  // }];
  var CONFIG = {
    entry: './sdk/vivar',
    output: {
      path: libPath,
      filename: 'vivar.min.js',
      library: 'Vivar',
      libraryTarget: 'umd'
    },
    devtool: devtool,
    module: {
      rules: [
      //   {
      //   test: /\.jsx?$/,
      //   loader: 'babel-loader',
      //   options: {
      //     presets: [
      //       { env: { modules: 'false' } }, babiliConfig]
      //   },
      //   exclude: ['/node_modules']
      // },
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
      ]
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
    },
    plugins: [
      new CleanWebpackPlugin(libPath),
      new webpack.LoaderOptionsPlugin({
        minimize: true
      }),
    ]
  };
  return CONFIG;
};
